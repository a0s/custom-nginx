CUSTOM-NGINX
============

```
                                        )                         
   (                 )               ( /(                         
   )\     (       ( /(         )     )\()) (  (  (             )  
 (((_)   ))\  (   )\()) (     (     ((_)\  )\))( )\   (     ( /(  
 )\___  /((_) )\ (_))/  )\    )\  '  _((_)((_))\((_)  )\ )  )\()) 
((/ __|(_))( ((_)| |_  ((_) _((_))  | \| | (()(_)(_) _(_/( ((_)\  
 | (__ | || |(_-<|  _|/ _ \| '  \() | .` |/ _` | | || ' \))\ \ /  
  \___| \_,_|/__/ \__|\___/|_|_|_|  |_|\_|\__, | |_||_||_| /_\_\  
                                          |___/                   
```

The very default Nginx compiled from the sources

How To Run
----------

```shell script
docker run \
  -p 8080:80 \
  registry.gitlab.com/a0s/custom-nginx:latest
```

You able to add a custom `server {}` section using docker volumes by mapping it inside `/etc/nginx/conf.d` folder. 
For e.g., if you have `my_site.conf` with custom `server {}` section you can map it like this:

```shell script
docker run \
  -p 8080:80 \
  -v my_site.conf:/etc/nginx/conf.d/my_site.conf \
  registry.gitlab.com/a0s/custom-nginx:latest
```

Security
--------

The image uses gid=10001 and uid=10001 by default for inner user `nginx`. This can be change with build args:

```shell script
docker build --tag custom-nginx --build-arg GID=<new gid> --build-arg UID=<new uid> .
```

Monitoring
----------

This nginx' image includes `http_stub_status_module`. You can get the statistic by requesting the server's ip with path `/monitoring` 

Example response:

```
Active connections: 1 
server accepts handled requests
 1 1 3 
Reading: 0 Writing: 1 Waiting: 0 
```

Let's see how can we get this statistic into Prometheus and Grafana:

0) Run custom-nginx as described above with exposed port 8080.

    ```shell script
    docker run \
        -p 8080:80 \
        --name custom-nginx \
        -v my_site.conf:/etc/nginx/conf.d/my_site.conf \
        registry.gitlab.com/a0s/custom-nginx:latest
    ```

1) The statistic could be export to Prometheus with [nginx-prometheus-exporter](https://github.com/nginxinc/nginx-prometheus-exporter). 
`nginx-prometheus-exporter` container should be able to connect to running nginx container (its very depends on your environment in which containers runs)

    ```shell script
    docker run \
        -p 9113:9113 \
        --name nginx-prometheus-exporter \
        nginx/nginx-prometheus-exporter:0.8.0 \
        -nginx.scrape-uri http://custom-nginx:8080/monitoring
    ```

2) Add a new section into Prometheus config and reload it:

    ```yaml
     - job_name: nginx-exporter
       scrape_interval: 5s
       static_configs:
         - targets:
             - nginx-prometheus-exporter:9113
   ```
   
3) Add a new Grafana diagram with metrics `stub_status` as described in [doc](https://github.com/nginxinc/nginx-prometheus-exporter)
