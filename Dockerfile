ARG ALPINE_VERSION=3.12.1
ARG NGINX_VERSION=1.19.4
ARG GID=10001
ARG UID=10001

FROM alpine:${ALPINE_VERSION} as builder
ARG ALPINE_VERSION
ARG NGINX_VERSION
LABEL \
  image-type=builder \
  nginx-version="${NGINX_VERSION}" \
  alpine-version="${ALPINE_VERSION}"

WORKDIR /opt/nginx
RUN \
  apk --no-cache add build-base pcre-dev zlib-dev git && \
  git clone https://github.com/nginx/nginx.git /opt/nginx && \
  git checkout release-${NGINX_VERSION} && \
  mkdir -p /usr/lib/nginx/modules && \
  ./auto/configure \
    --prefix=/opt/nginx-build \
    --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --http-client-body-temp-path=/var/cache/nginx/client_temp \
    --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
    --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
    --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
    --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
    --pid-path=/var/run/nginx/nginx.pid \
    --lock-path=/var/run/nginx/nginx.lock \
    --modules-path=/usr/lib/nginx/modules \
    --with-http_stub_status_module \
    --user=nginx \
    --group=nginx && \
    make && \
    make install



FROM alpine:${ALPINE_VERSION} as runner
ARG ALPINE_VERSION
ARG NGINX_VERSION
ARG GID
ARG UID
LABEL \
  image-type=runner \
  nginx-version="${NGINX_VERSION}" \
  alpine-version="${ALPINE_VERSION}"

COPY --from=builder /opt/nginx-build/conf /etc/nginx
COPY --from=builder /opt/nginx-build/html /usr/share/nginx/html
COPY --from=builder /opt/nginx-build/sbin/nginx /usr/sbin/nginx
COPY --from=builder /usr/lib/nginx/modules /usr/lib/nginx/modules
COPY nginx.conf /etc/nginx/nginx.conf

RUN \
  mkdir -p \
    /etc/nginx \
    /etc/nginx/conf.d \
    /usr/lib/nginx/modules \
    /usr/share/nginx/html \
    /var/cache/nginx/client_temp \
    /var/cache/nginx/fastcgi_temp \
    /var/cache/nginx/proxy_temp \
    /var/cache/nginx/scgi_temp \
    /var/cache/nginx/uwsgi_temp \
    /var/log/nginx \
    /var/run/nginx \
    && \
  apk --no-cache add pcre zlib && \
  addgroup -g ${GID} -S nginx && \
  adduser -D -S -u ${UID} -h /var/cache/nginx -s /sbin/nologin -G nginx nginx && \
  chown -R nginx:nginx \
    /etc/nginx \
    /usr/lib/nginx \
    /usr/sbin/nginx \
    /usr/share/nginx \
    /var/cache/nginx \
    /var/log/nginx \
    /var/run/nginx

USER nginx:nginx
EXPOSE 80
CMD ["/usr/sbin/nginx", "-c", "/etc/nginx/nginx.conf"]
